﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour {

    public GameObject explosion;
	// Use this for initialization
	void Start () {
        
        transform.rotation = Camera.main.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.forward/3);
	}

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.tag.Equals("Enemy"))
        {
            ((Enemy)coll.gameObject.GetComponent("Enemy")).AddToCurrentHealth(-20);
        }
        Instantiate(explosion, transform.position, transform.rotation);
        GetComponent<SphereCollider>().enabled = false;
        Destroy(gameObject);
    }

}
