﻿using UnityEngine;
using System.Collections;

public class IceBlast : MonoBehaviour {

    public GameObject explosion;
    public ParticleEmitter emitter;
    public ParticleEmitter emitter2;
    public ParticleSystem emitter3;
    private Vector3 direction;
    // Use this for initialization
    void Start()
    {

        transform.rotation = Camera.main.transform.rotation;
        direction = Vector3.forward;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction / 3);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag.Equals("Enemy"))
        {
            ((Enemy)coll.gameObject.GetComponent("Enemy")).AddToCurrentHealth(-20);
        }
        Instantiate(explosion, transform.position, transform.rotation);
        emitter.emit = false;
        emitter2.emit = false;
        emitter3.Stop();
        direction = Vector3.zero;
        GetComponent<Light>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        Destroy(gameObject,5f);
    }
}
