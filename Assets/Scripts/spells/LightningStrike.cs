﻿using UnityEngine;
using System.Collections;

public class LightningStrike : MonoBehaviour {

    private Player player;

    public LineRenderer lineRenderer;

    

	// Use this for initialization
	void Start () {
        player = (Player) GameObject.FindGameObjectWithTag("Player").GetComponent("Player");

        
        lineRenderer.SetPosition(0, player.Target.transform.position + new Vector3(0,1,0));

        lineRenderer.SetPosition(2, player.transform.GetChild(2).position);

        lineRenderer.SetPosition(1, (player.transform.GetChild(2).position + player.Target.transform.position + new Vector3(0, 1, 0)) / 2 
            + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)));

        

        //Destroy(gameObject, 1);        

        
	}
	
	// Update is called once per frame
	void Update () {

	}
}
