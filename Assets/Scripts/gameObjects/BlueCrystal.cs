﻿using UnityEngine;
using System.Collections;

public class BlueCrystal : MonoBehaviour, IInteractiveGameObject
{

    private bool used = false;
    private Player playerScript;
    public GameObject outline;


    // Use this for initialization
    void Start()
    {

    }

    void OnMouseEnter()
    {
        //if (!used)
        {
            outline.renderer.materials[0].SetFloat("_Outline", 0.008f);
        }
    }

    void OnMouseExit()
    {
        //if (!used)
        {
            outline.renderer.materials[0].SetFloat("_Outline", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Interact()
    {
        if (!used)
        {
            used = true;
            renderer.materials[0].SetColor("_Color", Color.black);
            renderer.materials[0].SetColor("_ReflectColor", Color.black);
            GetComponent<Light>().enabled = false;

            //outline.renderer.materials[0].SetFloat("_Outline", 0);
        }
        else
        {
            used = false;
            renderer.materials[0].SetColor("_Color", new Color(38/255f,0,1));
            renderer.materials[0].SetColor("_ReflectColor", new Color(38/255f, 0, 1));
            GetComponent<Light>().enabled = true;                
        }
    }
}
