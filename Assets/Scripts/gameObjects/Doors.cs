using UnityEngine;
using System.Collections;

public class Doors : MonoBehaviour, IInteractiveGameObject
{


    private bool opened = false;
    public Animation anim;
    private int numberOfMaterials;


    // Use this for initialization
    void Start()
    {
        numberOfMaterials = renderer.materials.Length;
    }


    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseEnter()
    {
        renderer.materials[0].SetFloat("_Outline", 0.008f);
        if (numberOfMaterials > 1)
            renderer.materials[1].SetFloat("_Outline", 0.008f);
    }

    void OnMouseExit()
    {
        renderer.materials[0].SetFloat("_Outline", 0);
        if (numberOfMaterials > 1)
            renderer.materials[1].SetFloat("_Outline", 0);
    }

    public void Interact()
    {
        if (opened)
        {
            if (!anim.isPlaying)
            {
                anim.Play("close");
                opened = !opened;
            }
        }
        else
        {
            if (!anim.isPlaying)
            {
                anim.Play("open");
                opened = !opened;
            }
        }
    }


}
