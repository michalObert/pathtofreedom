﻿using UnityEngine;
using System.Collections;

public interface IInteractiveGameObject {

    void Interact();
}
