﻿using UnityEngine;
using System.Collections;

public class MainCharacterGenerator : MonoBehaviour {
    Player player;
	// Use this for initialization
	void Start () {
        player = (Player)GameObject.FindGameObjectWithTag("Player").GetComponent("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI() {
        DisplayName();
        DisplayStats();
    }

    private void DisplayName() {
        GUI.Label(new Rect(10, 10, 50, 25), "Name:");
        player.Name = GUI.TextArea(new Rect(65, 10, 100, 25), player.Name);
    }

    private void DisplayStats() {

    }
}
