﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public string Name { get; set; }
    private int maxHealth;
    public int MaxHealth
    {
        get { return maxHealth; }
        set
        {
            maxHealth = value;
            gameGUI.PlayerMaxHealth = maxHealth;
        }
    }

    private int currentHealth;
    public int CurrentHealth
    {
        get { return currentHealth; }
        set
        {
            currentHealth = value;
            gameGUI.PlayerCurrentHealth = currentHealth;
        }
    }

    
    private GameGUI gameGUI;
    private float attackTimer;
    private float attackSpeed;
    private Enemy target;
    public Enemy Target
    {
        get { return target; }
        private set
        {
            target = value;
            gameGUI.Target = target;
        }
    }
    private int spell = 0;
    // Use this for initialization

    void Awake()
    {
        gameGUI = gameObject.AddComponent("GameGUI") as GameGUI;
    }

    void Start()
    {
        MaxHealth = 100;
        CurrentHealth = 100;
        attackSpeed = 1.5f;
        attackTimer = 0.0f;
        Target = null;
    }


    public void AddToCurrentHealth(int hpChange)
    {
        CurrentHealth += hpChange;

        if (CurrentHealth < 0)
        {
            CurrentHealth = 0;
        }

        if (CurrentHealth > MaxHealth)
        {
            CurrentHealth = MaxHealth;
        }

        if (MaxHealth < 1)
        { //but WHY whould you ever do that?...
            MaxHealth = 1;
        }
        
        gameGUI.PlayerCurrentHealth = CurrentHealth;
    }

    private void Attack()
    {
        Ray clickPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitPoint;
        if (Physics.Raycast(clickPoint, out hitPoint))
        {
            GameObject gameObject = hitPoint.transform.gameObject;
            if (gameObject.tag.Equals("Enemy") && Target != (Enemy)gameObject.GetComponent("Enemy"))
            {
                Target = (Enemy)gameObject.GetComponent("Enemy");
                Debug.Log("Enemy Targeted");
            }
        }
        if (Target != null)
        {
            Vector3 direction = (Target.transform.position - transform.position).normalized;
            float isFacingTarget = Vector3.Dot(direction, transform.forward);
            if (Vector3.Distance(transform.position, Target.transform.position) < 4 && isFacingTarget > 0)
            {
                bool isDead = Target.AddToCurrentHealth(-40);
                Debug.Log("Attacked");
                if (isDead)
                {
                    Debug.Log("Killed");
                    Target = null;
                }
            }
        }
    }

    private void CastSpell() {
        switch (spell)
        {
            case 0:
                Instantiate(Resources.Load("Prefabs/ParticalEffects/IceBlast"), transform.GetChild(2).position, transform.rotation);                
                break;
            case 1:
                Instantiate(Resources.Load("Prefabs/ParticalEffects/fireball"), transform.GetChild(2).position, transform.rotation);
                break;
            case 2:
                Instantiate(Resources.Load("Prefabs/ParticalEffects/LightningStrike"));
                break;
            default:
                break;
        }      
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            spell = 0;
            gameGUI.PlayerActiveSpellID = 0;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            spell = 1;
            gameGUI.PlayerActiveSpellID = 1;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            spell = 2;
            gameGUI.PlayerActiveSpellID = 2;
        }

        #region Attack
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }
        if (attackTimer < 0)
        {
            attackTimer = 0;
        }
        if (Input.GetMouseButtonDown(0) && attackTimer == 0)
        {
            Attack();
            attackTimer = attackSpeed;
        }
        #endregion

        #region spellCast
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }
        if (attackTimer < 0)
        {
            attackTimer = 0;
        }
        if (Input.GetMouseButtonDown(1) && attackTimer == 0)
        {
            CastSpell();
            attackTimer = attackSpeed;
        }
        #endregion


        
        if (Input.GetKeyDown(KeyCode.E))
        {
            OnEClick();
        }


    }

    void OnEClick()
    {
        // Cast a ray from the mouse
        // cursors position 
        Ray clickPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitPoint;

        // See if the ray collided with an object
        if (Physics.Raycast(clickPoint, out hitPoint))
        {

            GameObject go = hitPoint.transform.gameObject;
            if (Vector3.Distance(this.transform.position, go.transform.position) < 4)
            {

                if (go.GetComponent("IInteractiveGameObject") is IInteractiveGameObject)
                {
                    IInteractiveGameObject igo = (IInteractiveGameObject)go.GetComponent("IInteractiveGameObject");
                    igo.Interact();
                }

            }

        }
    }
}
