﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]//will use this for distance trigered event
public class Enemy : MonoBehaviour {

    public string Name { get; set; }
    private int maxHealth;

    public int MaxHealth {
        get { return maxHealth; }
        set { 
            maxHealth = value;
            HealthBarLength = Screen.width / 4 * (CurrentHealth / (float)MaxHealth);
        }
    }

    private int currentHealth;

    public int CurrentHealth {
        get { return currentHealth; }
        set { 
            currentHealth = value;
            HealthBarLength = Screen.width / 4 * (CurrentHealth / (float)MaxHealth);
        }
    }
    
    public float HealthBarLength { get; set;}
    public int DetectionRadius { get; set; }
    public float rotationSpeed;
    public float movementSpeed;
    public float attackSpeed;
    public float attackTimer;
    public int damage;


    private State state;
    private bool didFlee;
    private bool isAlive = true;
    private Quaternion defaultRotation;
    
    private Transform cachedTransform;
    private Transform home;
    private Player player;
    private Transform target;
    private SphereCollider sphereCollider;
    private CharacterController characterController;

    private enum State {
     //   Idle,
        Init,
        Setup,
        Search,
        Attack,
        Retreat,
        Flee
    }

    void Start() {
        state = State.Init;
        StartCoroutine("AdvancedBehaviour");
    }


    private void Move() {
        cachedTransform.rotation = Quaternion.Slerp(cachedTransform.rotation,
            Quaternion.LookRotation(target.position - cachedTransform.position),
            rotationSpeed * Time.deltaTime);
        //attack or move closer
        float distance = Vector3.Distance(cachedTransform.position, target.position);
        if(distance > 2.5) {
            cachedTransform.position += new Vector3(cachedTransform.forward.x * movementSpeed * Time.deltaTime, 
                0, cachedTransform.forward.z * movementSpeed * Time.deltaTime);
        }
    }

    private void UnconditionalMove() {
        cachedTransform.rotation = Quaternion.Slerp(cachedTransform.rotation,
            Quaternion.LookRotation(target.position - cachedTransform.position),
            rotationSpeed * Time.deltaTime);
        //attack or move closer
        float distance = Vector3.Distance(cachedTransform.position, target.position);
            cachedTransform.position += new Vector3(cachedTransform.forward.x * movementSpeed * Time.deltaTime,
                0, cachedTransform.forward.z * movementSpeed * Time.deltaTime);
    }
    void Update() {
        //look at target
        if(attackTimer > 0) {
            attackTimer -= Time.deltaTime;
        }

        if(attackTimer < 0) {
            attackTimer = 0;
        }
        if(target == null) {
            return;
        }
    }

    private IEnumerator AdvancedBehaviour() {
        while(isAlive || CurrentHealth != MaxHealth) {
            switch(state) {
                case State.Init:
                    Init();
                    break;
                case State.Setup:
                    Setup();
                    break;
                case State.Search:
                    Search();
                    break;
                case State.Attack:
                    Attack();
                    break;
                case State.Retreat:
                    Retreat();
                    break;
                case State.Flee:
                    Flee();
                    break;
            }
            yield return null;
        }
    }
    private void Init() {
        Debug.Log("INIT");
        sphereCollider = GetComponent<SphereCollider>();
        characterController = GetComponent<CharacterController>();        
        if(sphereCollider == null) {
            Debug.LogError("SphereCollider is missing!!!");
            isAlive = false;
            return;
        }
        player = (Player)GameObject.FindGameObjectWithTag("Player").GetComponent("Player");
        if(player == null) {
            Debug.LogError("Player is not in scene!");
            isAlive = false;
            return;
        }
        target = null;
        home = transform.parent.parent.transform;//spawnPoint
        state = State.Setup;
    }
    private void Setup() {
        Debug.Log("SETUP");
        DetectionRadius = 26;
        MaxHealth = 100;
        CurrentHealth = 100;
        HealthBarLength = Screen.width / 4;
        cachedTransform = transform;
        defaultRotation = cachedTransform.rotation; //new Vector4(cachedTransform.rotation.x, cachedTransform.rotation.y, cachedTransform.rotation.z,cachedTransform.rotation.w);
        sphereCollider.center = characterController.center;
        sphereCollider.radius = DetectionRadius;
        sphereCollider.isTrigger = true;
        didFlee = false;
        attackSpeed = 4.0f;
        attackTimer = 0.0f;
        rotationSpeed = 20.0f;
        movementSpeed = 4.0f;
        damage = 42;
        isAlive = false;
        state = State.Search;
    }

    private void Search() {
        Debug.Log("SEARCH");
        if(player.transform == target.transform) {
            state = State.Attack;
            Debug.Log("Attack");
        } else {
            if(CurrentHealth != MaxHealth) {
                CurrentHealth += MaxHealth / 100;
            } else {
                isAlive = false;
            }
        }
    }
    private void Attack() {
        if(MaxHealth / 5 >= currentHealth
            && Vector3.Distance(cachedTransform.position, home.position) > DetectionRadius - 1) { //far enough so he may actualy escape
                if(didFlee) { //you chased him to the corner
                    AttackTarget();
                    Move();
                } else {
                    didFlee = true;
                    state = State.Flee;
                    Debug.Log("Flee");
                }
        } else {
            AttackTarget();
            Move();
        }        
    }

    private void Retreat() {
        Debug.Log("Retreat");
        //target = home;
        Move();
        state = State.Search;
    }

    private void Flee() {
        target = home;
        float currentRotationSpeed = rotationSpeed;
       // rotationSpeed = 100.0f;
        UnconditionalMove();
      //  rotationSpeed = currentRotationSpeed;

        if(Vector3.Distance(cachedTransform.position, home.position) < 1.0f){
            didFlee = true;
            cachedTransform.eulerAngles = defaultRotation.eulerAngles;
            state = State.Search;
        }
    }
    public void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")) {
            Debug.Log("Player in range");
            target = other.transform;
            isAlive = true;
            StartCoroutine("AdvancedBehaviour");       
        }
    }

    public void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")) {
            target = home;
            isAlive = false;
        }
    }
    void OnMouseEnter()
    {
            renderer.materials[0].SetFloat("_Outline", 0.008f);
        
    }

    void OnMouseExit()
    {
            renderer.materials[0].SetFloat("_Outline", 0);        
    }

    public bool AddToCurrentHealth(int hpChange) {
        CurrentHealth += hpChange;
        if(CurrentHealth < 0) {
            CurrentHealth = 0;           
        }
        if(CurrentHealth > MaxHealth) {
            CurrentHealth = MaxHealth;
        }
        if(MaxHealth < 1) { //but WHY whould you ever do that?...
            MaxHealth = 1;
        }        
        if(CurrentHealth == 0) {
            Destroy(transform.root.gameObject);
            return true;
        }
        return false;
    }
    public bool AttackTarget() {
        if(target == null) {
            return false; //this should never be true
        }
        float distance = Vector3.Distance(cachedTransform.position, player.transform.position);        
        Vector3 direction = (player.transform.position - transform.position).normalized;
        float isFacingTarget = Vector3.Dot(direction, transform.forward);
        if(distance <= 2.5 && isFacingTarget > .84f && attackTimer == 0) {//almost right in front of you and close enough
                    attackTimer = attackSpeed;
                    player.AddToCurrentHealth(-damage);
                    return true;
        }
        return false;
    }
    // Update is called once per frame
}
