﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour
{


    private Texture healthTexture;
    private Texture healthBoarder;
    private Texture spellTexture;
    private Texture spellBoarder;
    public int healthBarLength;
    public int PlayerMaxHealth { get; set; }
    public int PlayerCurrentHealth { get; set; }
    public int playerHealthPosition;
    public int PlayerActiveSpellID
    {
        get { return PlayerActiveSpellID; }
        set
        {            
            switch (value)
            {
                case 0:
                    spellTexture = (Texture)Resources.Load("Textures/iceblast");
                    break;
                case 1:
                    spellTexture = (Texture)Resources.Load("Textures/fireball");
                    break;
                case 2:
                    spellTexture = (Texture)Resources.Load("Textures/lightning");
                    break;
                default:
                    break;
            }
        }
    }
    //public float TargetHealthBarLength { get; set; }
    //public int TargetMaxHealth { get; set; }
    //public int TargetCurrentHealth { get; set; }
    public Enemy Target;

    private int targetHealthPosition;
    void Start()
    {
        healthBarLength = 250;
        targetHealthPosition = Screen.width - 10 - healthBarLength;
        playerHealthPosition = 10;
        healthTexture = (Texture)Resources.Load("Textures/healthTexture");
        healthBoarder = (Texture)Resources.Load("Textures/heatlthBarBoarder");
        spellBoarder = (Texture)Resources.Load("Textures/spellIconBoarder");
        PlayerActiveSpellID = 0;
    }

    void OnGUI()
    {

        GUI.DrawTexture(new Rect(playerHealthPosition, 10, healthBarLength * PlayerCurrentHealth / PlayerMaxHealth, 20), healthTexture);
        GUI.DrawTexture(new Rect(playerHealthPosition - 2, 8, healthBarLength + 4, 24), healthBoarder);
        GUI.Box(new Rect(playerHealthPosition, 10, healthBarLength, 20), PlayerCurrentHealth + "/" + PlayerMaxHealth);
        DisplayTargetHealthbar();

        GUI.DrawTexture(new Rect(10, Screen.height - 42, 32, 32), spellTexture);
        GUI.DrawTexture(new Rect(9, Screen.height - 43, 34, 34), spellBoarder);
    }

    void DisplayTargetHealthbar()
    {
        if (Target == null)
        {
            return;
        }
        GUI.DrawTexture(new Rect(targetHealthPosition + (healthBarLength - healthBarLength * Target.CurrentHealth / Target.MaxHealth), 10, healthBarLength * Target.CurrentHealth / Target.MaxHealth, 20), healthTexture);
        GUI.DrawTexture(new Rect(targetHealthPosition - 2, 8, healthBarLength + 4, 24), healthBoarder);
        GUI.Box(new Rect(targetHealthPosition, 10, healthBarLength, 20), Target.CurrentHealth + "/" + Target.MaxHealth);
    }

    void Update()
    {
    }
}
