﻿Shader "Custom/ReflectSpec" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
	_ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }
    _GlowTex ("Glow", 2D) = "" {}
    _GlowColor ("Glow Color", Color)  = (1,1,1,1)
    _GlowStrength ("Glow Strength", Range(0.0,6.0)) = 1.0
	[MaterialToggle] _Glow("Glow", float) = 0
}
SubShader {
	Tags {  }

CGPROGRAM
#pragma surface surf BlinnPhong

sampler2D _MainTex;
samplerCUBE _Cube;

fixed4 _Color;
fixed4 _ReflectColor;
half _Shininess;

float4 _GlowColor;
float _GlowStrength;
float _Glow;

struct Input {
	float2 uv_MainTex;
	float3 worldRefl;
	float3 viewDir;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 c = tex * _Color;
	o.Albedo = c.rgb;
	o.Gloss = tex.a;
	o.Specular = _Shininess;
	
	fixed4 reflcol = texCUBE (_Cube, IN.worldRefl);
	reflcol *= tex.a;
	if(_Glow == 1){
	half glow = 1.0 - saturate(dot(normalize(IN.viewDir),o.Normal));
	o.Emission =  _GlowColor.rgb * pow(glow,_GlowStrength);
	o.Alpha = reflcol.a * _ReflectColor.a ;
	} else {
	o.Emission = reflcol.rgb * _ReflectColor.rgb;
	o.Alpha = reflcol.a * _ReflectColor.a;
	}
}
ENDCG
}

FallBack "Diffuse"
}
