﻿Shader "Custom/DiffuseGlow" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) RefStrength (A)", 2D) = "white" {} 
    _GlowColor ("Glow Color", Color)  = (1,1,1,1)
    _GlowStrength ("Glow Strength", Range(0.0,6.0)) = 1.0
	[MaterialToggle] _Glow("Glow", float) = 0
}
SubShader {
	Tags {"Render Type" = "Opaque"}
	
CGPROGRAM
#pragma surface surf BlinnPhong

sampler2D _MainTex;
float _Glow;
float _GlowStrength;
float4 _GlowColor;
fixed4 _Color;

struct Input {
	float2 uv_MainTex;
	float3 worldRefl;
	float3 viewDir;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 c = tex * _Color;
	o.Albedo = c.rgb;

	if(_Glow == 1){
	half glow = 1.0 - saturate(dot(normalize(IN.viewDir),o.Normal));
	o.Emission =  _GlowColor.rgb * pow(glow,_GlowStrength);
	
	} 
	
}
ENDCG
}
	
FallBack "Diffuse"
} 
